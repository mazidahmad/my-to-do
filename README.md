# My To Do

This project is to fulfill submission of Netzme Take Home Test

## Getting Started

This project is based on Flutter 3.13.6

To run and build this project, use command line below :

    flutter pub get
    flutter pub run build_runner build --delete-conflicting-outputs

This project implement Clean Architecture for more maintainable, readable, and agile and use Cubit State Management.

I create this app design reference to **[Figma](https://www.figma.com/community/file/1113091900592641257/ui-kit-task-management?searchSessionId=lo42mnt0-9rfnjgnvr3)**

Demo and Result Folder : **[Google Drive](https://drive.google.com/drive/folders/1LgLz5UAyvGr5Q4HIGc4KvnWTlAIwrviE?usp=sharing)**

Thank You
