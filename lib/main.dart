import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:my_to_do/app.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:my_to_do/core/config/env.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async => runZonedGuarded(
      () async {
        WidgetsFlutterBinding.ensureInitialized();
        configureDependencies();
        await Hive.initFlutter();
        await Supabase.initialize(
            url: Env.supabaseUrl, anonKey: Env.supabaseKey);

        return runApp(const MyApp());
      },
      (error, stack) {
        log(error.toString(), stackTrace: stack);
      },
    );
