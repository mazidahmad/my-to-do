import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateUtil {
  static String getFormattedDate(DateTime date) {
    DateFormat df = DateFormat('MMM dd, yyyy');
    return df.format(date).toString();
  }

  static String getSimpleFormattedDate(DateTime date) {
    DateFormat df = DateFormat('yyyy-MM-dd');
    return df.format(date).toString();
  }

  static DateTime getDateWithoutTime(DateTime date) {
    DateFormat df = DateFormat('yyyy-MM-dd');
    return DateTime.parse(df.format(date).toString());
  }

  static String getDateCategory(DateTime date) {
    DateFormat df = DateFormat('yyyy-MM-dd');
    DateTime currDate = DateTime.now();

    DateTime date1 = DateTime.parse(df.format(currDate).toString());
    DateTime date2 = DateTime.parse(df.format(date).toString());

    if (date2.isBefore(date1)) {
      return 'yesterday';
    } else if (date2.isAfter(date1)) {
      return 'tomorrow';
    } else {
      return 'today';
    }
  }

  static bool isYesterday(DateTime date) =>
      getDateCategory(date) == 'yesterday';
  static bool isTomorrow(DateTime date) => getDateCategory(date) == 'tomorrow';
  static bool isToday(DateTime date) => getDateCategory(date) == 'today';

  static IconData getIconStatusByDate(DateTime date, bool isCompleted) {
    switch (getDateCategory(date)) {
      case 'yesterday':
        return Icons.keyboard_double_arrow_right_rounded;
      case 'tomorrow':
        return Icons.keyboard_double_arrow_left_rounded;
      default:
        if (isCompleted) return Icons.keyboard_double_arrow_up_rounded;
        return Icons.keyboard_double_arrow_down_rounded;
    }
  }
}
