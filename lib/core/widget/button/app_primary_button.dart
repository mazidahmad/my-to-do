import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/theme/theme.dart';

class AppPrimaryButton extends StatelessWidget {
  const AppPrimaryButton({
    super.key,
    this.text,
    this.backgroundColor,
    this.onPressed,
    this.prefixIcon,
  });

  final String? text;
  final void Function()? onPressed;
  final Color? backgroundColor;
  final String? prefixIcon;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        backgroundColor: backgroundColor,
        minimumSize: Size(double.infinity, 56.h),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (prefixIcon != null)
            Row(
              children: [
                SvgPicture.asset(prefixIcon!),
                Gap(10.w),
              ],
            ),
          Text(
            text ?? 'Button',
            style: AppTextStyle.headlineMedium(color: Colors.white),
          ),
        ],
      ),
    );
  }
}
