import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/features/home/presentation/cubit/home_cubit.dart';
import 'package:my_to_do/router/app_router.dart';
import 'package:my_to_do/router/app_router.gr.dart';

class AppFloatingButton extends StatelessWidget {
  const AppFloatingButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => getIt<AppRouter>().push(CreateTodoRoute()).then(
            (value) => context.read<HomeCubit>()
              ..geTodayTodoList()
              ..getTomorrowTodoList(),
          ),
      child: Container(
        margin: EdgeInsets.all(4.sp),
        width: 60.w,
        height: 60.h,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          gradient: AppColors.gradient1,
        ),
        child: Icon(
          Icons.add_rounded,
          size: 40.sp,
          color: Colors.white,
        ),
      ),
    );
  }
}
