class AppAsset {
  static const String onBoardingIllustration =
      'assets/images/illustrations/on-boarding.svg';
  static const String myTodoLogo = 'assets/images/my-todo-logo.svg';
  static const String appleIcon = 'assets/images/icons/apple-icon.svg';
  static const String googleIcon = 'assets/images/icons/google-icon.svg';
  static const String homeIcon = 'assets/images/icons/home-icon.svg';
  static const String calendarIcon = 'assets/images/icons/calendar-icon.svg';
}
