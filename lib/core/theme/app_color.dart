import 'package:flutter/material.dart';

class AppColors {
  // Global
  static const Color primaryColor = Color(0xFF4B7BE5);
  static const Color secondaryColor = Color(0xFF88A2E7);
  static const Color backgroundColor = Color(0xFFFFFFFF);
  static const Color surfaceColor = Color(0xFFF8F6FE);
  static const Color activeIconColor = primaryColor;
  static const Color inactiveIconColor = secondaryColor;
  static const Color textfieldBackgroundColor = Color(0xFFF5F5F5);
  static const Color hintTextColor = Color(0xFFA9A9A9);
  static const Color textColor = Color(0xFF363942);
  static const Color lighterTextColor = Color(0xFF959699);
  static const Color secondaryTextColor = Color(0xFFFFFFFF);
  static const Color green = Color(0xFF93CEC7);
  static const Color red = Color(0xFFE47591);

  //Gradient
  static const LinearGradient gradient1 =
      LinearGradient(colors: [Color(0xFF0093E9), Color(0xFF80D0C7)]);
}
