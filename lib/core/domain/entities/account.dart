import 'package:equatable/equatable.dart';

class Account extends Equatable {
  final String displayName;
  final String email;
  final String profilePicture;
  final String idToken;

  const Account({
    required this.displayName,
    required this.email,
    required this.profilePicture,
    required this.idToken,
  });

  Account copyWith({
    String? displayName,
    String? email,
    String? profilePicture,
    String? idToken,
  }) {
    return Account(
      displayName: displayName ?? this.displayName,
      email: email ?? this.email,
      profilePicture: profilePicture ?? this.profilePicture,
      idToken: idToken ?? this.idToken,
    );
  }

  @override
  List<Object> get props => [displayName, email, profilePicture, idToken];
}
