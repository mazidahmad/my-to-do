import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:my_to_do/core/log/filter/release_log_filter.dart';
import 'package:my_to_do/core/log/printer/simple_log_printer.dart';
import 'package:my_to_do/core/network/supabase_module.dart';
// import 'package:supabase_flutter/supabase_flutter.dart';

@module
abstract class RegisterModule {
  Logger get logger => Logger(
        printer: SimpleLogPrinter(),
        filter: ReleaseLogFilter(),
      );

  GoogleSignIn get googleSignIn => GoogleSignIn();

  SupabaseModule get supabaseModule => SupabaseModule.initialize();
}
