import 'dart:convert';

import 'package:my_to_do/core/domain/entities/account.dart';

class AccountModel extends Account {
  const AccountModel(
      {required super.displayName,
      required super.email,
      required super.profilePicture,
      required super.idToken});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'displayName': displayName,
      'email': email,
      'profilePicture': profilePicture,
      'idToken': idToken,
    };
  }

  factory AccountModel.fromMap(Map<String, dynamic> map) {
    return AccountModel(
      displayName: map['displayName'] as String,
      email: map['email'] as String,
      profilePicture: map['profilePicture'] as String,
      idToken: map['idToken'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory AccountModel.fromJson(String source) =>
      AccountModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;
}
