import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/data/models/account_model.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/storage/secure_storage.dart';

abstract class LocalDatasource {
  Future<AccountModel?> getCurrentUser();
  Future<void> saveLoginSession(AccountModel data);
}

@Injectable(as: LocalDatasource)
class LocalDatasourceImpl extends LocalDatasource {
  final SecureStorage _secureStorage = getIt<SecureStorage>();

  @override
  Future<AccountModel?> getCurrentUser() async {
    var data = await _secureStorage.secureRead('auth_data');
    if (data == null) return null;

    var accountData = AccountModel.fromJson(data);
    return accountData;
  }

  @override
  Future<void> saveLoginSession(AccountModel data) async {
    await _secureStorage.secureSave('auth_data', data.toJson());
  }
}
