import 'package:auto_route/auto_route.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/features/home/presentation/pages/home_page.dart';
import 'package:my_to_do/features/onboard/presentation/pages/on_boarding_page.dart';
import 'package:my_to_do/features/startup/presentation/pages/startup_page.dart';
import 'package:my_to_do/features/todo/presentation/pages/create_todo_page.dart';
import 'package:my_to_do/router/app_router.gr.dart';

@lazySingleton
@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          page: StartupRoute.page,
          path: StartupPage.routeName,
          initial: true,
        ),
        AutoRoute(
          page: OnBoardingRoute.page,
          path: OnBoardingPage.routeName,
        ),
        AutoRoute(
          page: HomeRoute.page,
          path: HomePage.routeName,
        ),
        AutoRoute(
          page: CreateTodoRoute.page,
          path: CreateTodoPage.routeName,
        ),
      ];
}
