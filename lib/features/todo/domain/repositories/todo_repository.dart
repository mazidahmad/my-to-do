import 'package:dartz/dartz.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';

abstract class TodoRepository {
  Future<Either<Failure, void>> createTodo(Todo todo);
  Future<Either<Failure, void>> updateTodo(Todo todo);
}
