// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import 'package:my_to_do/features/todo/data/models/todo_model.dart';

class Todo extends Equatable {
  final int id;
  final String email;
  final String title;
  final DateTime date;
  final String? description;
  final bool isCompleted;

  const Todo({
    this.id = 0,
    this.email = '',
    required this.title,
    required this.date,
    required this.isCompleted,
    this.description,
  });

  TodoModel toModel() => TodoModel(
      id: id,
      email: email,
      title: title,
      date: date,
      description: description,
      isCompleted: isCompleted);

  @override
  List<Object?> get props {
    return [
      id,
      email,
      title,
      date,
      description,
      isCompleted,
    ];
  }

  Todo copyWith({
    int? id,
    String? email,
    String? title,
    DateTime? date,
    String? description,
    bool? isCompleted,
  }) {
    return Todo(
      id: id ?? this.id,
      email: email ?? this.email,
      title: title ?? this.title,
      date: date ?? this.date,
      description: description ?? this.description,
      isCompleted: isCompleted ?? this.isCompleted,
    );
  }
}
