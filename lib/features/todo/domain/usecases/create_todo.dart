import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/features/todo/domain/repositories/todo_repository.dart';

@Injectable()
class CreateTodo {
  final TodoRepository _repo = getIt<TodoRepository>();

  Future<Either<Failure, void>> execute(Todo todo) => _repo.createTodo(todo);
}
