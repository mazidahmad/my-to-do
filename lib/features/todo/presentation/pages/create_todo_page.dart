import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/core/utils/date_util.dart';
import 'package:my_to_do/core/widget/button/app_primary_button.dart';
import 'package:my_to_do/features/home/presentation/widget/app_text_form_field.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/features/todo/presentation/cubit/create_todo_cubit.dart';
import 'package:my_to_do/router/app_router.dart';

@RoutePage()
class CreateTodoPage extends StatefulWidget {
  const CreateTodoPage({super.key, this.todo});

  static const String routeName = '/create-todo';

  final Todo? todo;

  @override
  State<CreateTodoPage> createState() => _CreateTodoPageState();
}

class _CreateTodoPageState extends State<CreateTodoPage> {
  late final CreateTodoCubit _cubit;
  DateTime todoDate = DateTime.now();

  @override
  void initState() {
    _cubit = getIt<CreateTodoCubit>();
    _cubit.dateController.text = DateUtil.getFormattedDate(todoDate);
    super.initState();
  }

  @override
  void dispose() {
    _cubit.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.todo != null ? 'Detail To Do' : 'Create To Do'),
      ),
      body: BlocProvider(
        create: (context) => _cubit..initTodo(widget.todo),
        child: BlocListener<CreateTodoCubit, CreateTodoState>(
          listener: (context, state) {
            if (state is CreateTodoSuccess) {
              getIt<AppRouter>().pop();
            }
          },
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Form(
                key: _cubit.formKey,
                child: Column(
                  children: [
                    AppTextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Title required';
                        }
                        return null;
                      },
                      controller: _cubit.titleController,
                      label: 'Title',
                      hint: 'Title of To Do',
                      isReadOnly: widget.todo?.isCompleted,
                    ),
                    Gap(32.h),
                    AppTextFormField(
                      controller: _cubit.dateController,
                      label: 'Date',
                      hint: 'Date of To Do',
                      isReadOnly: true,
                      onTap: () async {
                        if (widget.todo != null
                            ? !(widget.todo?.isCompleted ?? true)
                            : true) {
                          await showModalBottomSheet(
                            backgroundColor: Colors.transparent,
                            context: context,
                            builder: (context) => Container(
                              height: 300.h,
                              decoration: BoxDecoration(
                                color: AppColors.backgroundColor,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.sp),
                                  topRight: Radius.circular(20.sp),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20.w, vertical: 25.h),
                                    child: Text(
                                      'Date To Do',
                                      style: AppTextStyle.headlineLarge(),
                                    ),
                                  ),
                                  Expanded(
                                    child: CupertinoDatePicker(
                                      onDateTimeChanged: (value) {
                                        _cubit.dateController.text =
                                            DateUtil.getFormattedDate(value);
                                        todoDate = value;
                                        setState(() {});
                                      },
                                      initialDateTime: todoDate,
                                      minimumDate: DateUtil.getDateWithoutTime(
                                          DateTime.now()),
                                      minimumYear: DateTime.now().year,
                                      mode: CupertinoDatePickerMode.date,
                                      use24hFormat: true,
                                      showDayOfWeek: true,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }
                      },
                    ),
                    Gap(32.h),
                    AppTextFormField(
                      isReadOnly: widget.todo?.isCompleted,
                      controller: _cubit.descriptionController,
                      label: 'Description',
                      hint: 'Description of To Do (Optional)',
                      isExpand: true,
                    ),
                    const Spacer(),
                    Visibility(
                      visible: (widget.todo != null)
                          ? (!widget.todo!.isCompleted)
                          : true,
                      child: AppPrimaryButton(
                        text: widget.todo != null ? 'Edit' : 'Create',
                        onPressed: () {
                          if (_cubit.formKey.currentState!.validate()) {
                            if (widget.todo != null) {
                              _cubit.updateTodo(widget.todo!, todoDate);
                            } else {
                              _cubit.createTodo(todoDate);
                            }
                          }
                        },
                      ),
                    ),
                    Gap(32.h),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
