import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/core/utils/date_util.dart';
import 'package:my_to_do/features/home/domain/usecases/update_todo.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/features/todo/domain/usecases/create_todo.dart';

part 'create_todo_state.dart';

@Injectable()
class CreateTodoCubit extends Cubit<CreateTodoState> {
  CreateTodoCubit() : super(CreateTodoInitial());

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final CreateTodo _createTodo = getIt<CreateTodo>();
  final UpdateTodo _updateTodo = getIt<UpdateTodo>();

  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController dateController = TextEditingController();

  void createTodo(DateTime date) async {
    emit(CreateTodoLoading());

    var result = await _createTodo.execute(
      Todo(
        title: titleController.text,
        date: date,
        isCompleted: false,
        description: descriptionController.text,
      ),
    );

    emit(
      result.fold(
        (failure) => CreateTodoFailure(failure: failure),
        (data) => CreateTodoSuccess(),
      ),
    );
  }

  void initTodo(Todo? todo) async {
    emit(CreateTodoLoading());

    if (todo != null) {
      titleController.text = todo.title;
      dateController.text = DateUtil.getFormattedDate(todo.date);
      if (todo.description != null) {
        descriptionController.text = todo.description!;
      }
    }

    emit(CreateTodoInitial());
  }

  void updateTodo(Todo todo, DateTime date) async {
    emit(CreateTodoLoading());

    var result = await _updateTodo.execute(
      todo.copyWith(
        title: titleController.text,
        date: date,
        description: descriptionController.text,
      ),
    );

    emit(
      result.fold(
        (failure) => CreateTodoFailure(failure: failure),
        (data) => CreateTodoSuccess(),
      ),
    );
  }

  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    dateController.dispose();
  }
}
