part of 'create_todo_cubit.dart';

class CreateTodoState extends Equatable {
  const CreateTodoState();

  @override
  List<Object> get props => [];
}

class CreateTodoInitial extends CreateTodoState {}

class CreateTodoLoading extends CreateTodoState {}

class CreateTodoSuccess extends CreateTodoState {}

class CreateTodoFailure extends CreateTodoState {
  final Failure failure;

  const CreateTodoFailure({required this.failure});
}
