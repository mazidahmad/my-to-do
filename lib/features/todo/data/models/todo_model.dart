import 'dart:convert';

import 'package:my_to_do/features/todo/domain/entities/todo.dart';

class TodoModel extends Todo {
  const TodoModel({
    required super.id,
    required super.email,
    required super.title,
    required super.date,
    required super.isCompleted,
    super.description,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'email': email,
      'title': title,
      'date': date.toLocal().toIso8601String(),
      'description': description,
      'is_completed': isCompleted,
    };
  }

  factory TodoModel.fromMap(Map<String, dynamic> map) {
    return TodoModel(
      id: map['id'] as int,
      email: map['email'] as String,
      title: map['title'] as String,
      date: DateTime.parse(map['date'] as String),
      isCompleted: map['is_completed'] as bool,
      description:
          map['description'] != null ? map['description'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory TodoModel.fromJson(String source) =>
      TodoModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
