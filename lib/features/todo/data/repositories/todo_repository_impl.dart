import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/core/mixin/repository_mixin.dart';
import 'package:my_to_do/features/todo/data/datasources/todo_remote_datasource.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/features/todo/domain/repositories/todo_repository.dart';

@Injectable(as: TodoRepository)
class TodoRepositoryImpl extends TodoRepository with RepositoryMixin {
  final ToDoRemoteDatasource _remoteDatasource = getIt<ToDoRemoteDatasource>();

  @override
  Future<Either<Failure, void>> createTodo(Todo todo) =>
      callDataSource(() => _remoteDatasource.createTodo(todo.toModel()));

  @override
  Future<Either<Failure, void>> updateTodo(Todo todo) =>
      callDataSource(() => _remoteDatasource.createTodo(todo.toModel()));
}
