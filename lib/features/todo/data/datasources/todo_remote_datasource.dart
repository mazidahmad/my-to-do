import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/data/datasources/local_datasource.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/network/supabase_module.dart';
import 'package:my_to_do/features/todo/data/models/todo_model.dart';

abstract class ToDoRemoteDatasource {
  Future<void> createTodo(TodoModel todo);
  Future<void> updateTodo(TodoModel todo);
}

@Injectable(as: ToDoRemoteDatasource)
class ToDoRemoteDatasourceImpl extends ToDoRemoteDatasource {
  final SupabaseModule _supabase = getIt<SupabaseModule>();
  final LocalDatasource _localDatasource = getIt<LocalDatasource>();

  @override
  Future<void> createTodo(TodoModel todo) async {
    var account = await _localDatasource.getCurrentUser();

    todo = todo.copyWith(email: account!.email).toModel();
    await _supabase.client.from('to_do_table').insert(todo.toMap());
  }

  @override
  Future<void> updateTodo(TodoModel todo) async {
    await _supabase.client
        .from('to_do_table')
        .update(todo.toMap())
        .eq('id', todo.id);
  }
}
