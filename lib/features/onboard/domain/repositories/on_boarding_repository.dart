import 'package:dartz/dartz.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/error/failures.dart';

abstract class OnBoardingRepository {
  Future<Either<Failure, Account>> signInWithGoogle();
}
