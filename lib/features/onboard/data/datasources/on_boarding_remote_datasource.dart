import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/network/supabase_module.dart';

abstract class OnBoardingRemoteDatasource {
  Future<GoogleSignInAccount?> authenticateGoogle();
}

@Injectable(as: OnBoardingRemoteDatasource)
class OnBoardingRemoteDatasourceImpl extends OnBoardingRemoteDatasource {
  final GoogleSignIn _googleClient = getIt<GoogleSignIn>();
  final SupabaseModule supabase = getIt<SupabaseModule>();

  @override
  Future<GoogleSignInAccount?> authenticateGoogle() async {
    _googleClient.disconnect();
    return _googleClient.signIn();
  }
}
