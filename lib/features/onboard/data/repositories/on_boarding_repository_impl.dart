import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/data/datasources/local_datasource.dart';
import 'package:my_to_do/core/data/models/account_model.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/core/mixin/repository_mixin.dart';
import 'package:my_to_do/features/onboard/data/datasources/on_boarding_remote_datasource.dart';
import 'package:my_to_do/features/onboard/domain/repositories/on_boarding_repository.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

@Injectable(as: OnBoardingRepository)
class OnBoardingRepositoryImpl extends OnBoardingRepository
    with RepositoryMixin {
  final OnBoardingRemoteDatasource _remoteDatasource =
      getIt<OnBoardingRemoteDatasource>();
  final LocalDatasource _localDatasource = getIt<LocalDatasource>();

  @override
  Future<Either<Failure, Account>> signInWithGoogle() =>
      callDataSource(() async {
        var googleAccount = await _remoteDatasource.authenticateGoogle();
        if (googleAccount != null) {
          AccountModel account = AccountModel(
              displayName: googleAccount.displayName ?? '-',
              email: googleAccount.email,
              profilePicture: googleAccount.photoUrl ?? '-',
              idToken: '-');

          await _localDatasource.saveLoginSession(account);
          return account;
        } else {
          throw const AuthException('Something went error when authentication');
        }
      });
}
