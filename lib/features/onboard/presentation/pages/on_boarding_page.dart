import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/core/widget/button/app_primary_button.dart';
import 'package:my_to_do/features/onboard/presentation/cubit/on_boarding_cubit.dart';
import 'package:my_to_do/router/app_router.dart';
import 'package:my_to_do/router/app_router.gr.dart';

@RoutePage()
class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({super.key});

  static const String routeName = "/on-boarding";

  @override
  State<OnBoardingPage> createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  late final OnBoardingCubit _cubit;

  @override
  void initState() {
    _cubit = getIt<OnBoardingCubit>();
    super.initState();
  }

  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocListener<OnBoardingCubit, OnBoardingState>(
          listener: (context, state) {
            if (state is OnBoardingSuccessLogin) {
              getIt<AppRouter>().replace(HomeRoute(account: state.account));
            }
          },
          child: SafeArea(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            AppAsset.onBoardingIllustration,
                          ),
                          Gap(40.h),
                          Text(
                            'Enjoy Your Time',
                            style: AppTextStyle.displayLarge(),
                          ),
                          Gap(30.h),
                          Text(
                            'When you are confused about\nmanaging your task come to us',
                            textAlign: TextAlign.center,
                            style: AppTextStyle.bodyMediumHigh(
                                color: AppColors.textColor.withAlpha(150)),
                          ),
                          Gap(140.h),
                          AppPrimaryButton(
                            onPressed: () {
                              _cubit.loginWithGoogle();
                            },
                            text: 'Login with Google',
                            prefixIcon: AppAsset.googleIcon,
                          ),
                          Gap(25.h),
                          AppPrimaryButton(
                            onPressed: () {},
                            backgroundColor: AppColors.secondaryColor,
                            text: 'Login with Apple',
                            prefixIcon: AppAsset.appleIcon,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
