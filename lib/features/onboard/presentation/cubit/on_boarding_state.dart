part of 'on_boarding_cubit.dart';

sealed class OnBoardingState extends Equatable {
  const OnBoardingState();

  @override
  List<Object> get props => [];
}

final class OnBoardingInitial extends OnBoardingState {}

final class OnBoardingLoading extends OnBoardingState {}

final class OnBoardingSuccessLogin extends OnBoardingState {
  final Account account;

  const OnBoardingSuccessLogin({required this.account});
}

final class OnBoardingFailed extends OnBoardingState {
  final Failure failure;

  const OnBoardingFailed({required this.failure});
}
