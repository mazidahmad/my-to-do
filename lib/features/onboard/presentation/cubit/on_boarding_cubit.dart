import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/onboard/domain/usecases/login_with_google.dart';

part 'on_boarding_state.dart';

@Injectable()
class OnBoardingCubit extends Cubit<OnBoardingState> {
  OnBoardingCubit() : super(OnBoardingInitial());

  final LoginWithGoogle _loginWithGoogle = getIt<LoginWithGoogle>();

  void loginWithGoogle() async {
    emit(OnBoardingLoading());

    var result = await _loginWithGoogle.execute();

    emit(
      result.fold(
        (failure) => OnBoardingFailed(failure: failure),
        (data) => OnBoardingSuccessLogin(account: data),
      ),
    );
  }
}
