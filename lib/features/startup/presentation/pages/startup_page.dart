import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/theme/app_asset.dart';
import 'package:my_to_do/core/theme/app_text_style.dart';
import 'package:my_to_do/features/startup/presentation/cubit/startup_cubit.dart';
import 'package:my_to_do/router/app_router.dart';
import 'package:my_to_do/router/app_router.gr.dart';

@RoutePage()
class StartupPage extends StatefulWidget {
  const StartupPage({Key? key}) : super(key: key);

  static const String routeName = '/startup';

  @override
  State<StartupPage> createState() => _StartupPageState();
}

class _StartupPageState extends State<StartupPage> {
  late final StartupCubit _cubit;

  @override
  void initState() {
    _cubit = getIt<StartupCubit>();
    super.initState();
  }

  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit..checkSession(),
        child: BlocListener<StartupCubit, StartupState>(
          listener: (context, state) async {
            await Future.delayed(const Duration(seconds: 2));
            if (state is StartupLogedIn) {
              getIt<AppRouter>().replace(HomeRoute(account: state.account));
            }
            if (state is StartupNotLoginYet) {
              getIt<AppRouter>().replace(const OnBoardingRoute());
            }
          },
          child: SafeArea(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset(
                    AppAsset.myTodoLogo,
                    width: 120.w,
                    height: 120.h,
                  ),
                  Gap(30.h),
                  Text(
                    'My Todo',
                    style: AppTextStyle.displayLarge(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
