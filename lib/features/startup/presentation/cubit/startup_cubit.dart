import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/startup/domain/usecases/get_account_session.dart';
import 'package:my_to_do/features/startup/domain/usecases/sync_data.dart';

part 'startup_state.dart';

@Injectable()
class StartupCubit extends Cubit<StartupState> {
  StartupCubit() : super(StartupInitial());

  final GetAcccountSession _getAcccountSession = getIt<GetAcccountSession>();
  final SyncData _syncData = getIt<SyncData>();

  void checkSession() async {
    emit(StartupLoading());

    var result = await _getAcccountSession.execute();

    emit(
      await result.fold(
        (failure) => StartupFailed(failure: failure),
        (data) async {
          if (data == null) {
            return StartupNotLoginYet();
          } else {
            await _syncData.execute();
            return StartupLogedIn(account: data);
          }
        },
      ),
    );
  }
}
