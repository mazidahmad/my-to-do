// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'startup_cubit.dart';

class StartupState extends Equatable {
  const StartupState();

  @override
  List<Object> get props => [];
}

class StartupInitial extends StartupState {}

class StartupLoading extends StartupState {}

class StartupLogedIn extends StartupState {
  final Account account;

  const StartupLogedIn({required this.account});
}

class StartupNotLoginYet extends StartupState {}

class StartupFailed extends StartupState {
  final Failure failure;
  const StartupFailed({
    required this.failure,
  });
}
