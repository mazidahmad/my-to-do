import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/data/datasources/local_datasource.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/core/mixin/repository_mixin.dart';
import 'package:my_to_do/features/startup/data/datasources/startup_remote_datasource.dart';
import 'package:my_to_do/features/startup/domain/repositories/startup_repository.dart';

@Injectable(as: StartupRepository)
class StartupRepositoryImpl extends StartupRepository with RepositoryMixin {
  final LocalDatasource _localDatasource = getIt<LocalDatasource>();
  final SrtartupRemoteDatasource _remoteDatasource =
      getIt<SrtartupRemoteDatasource>();

  @override
  Future<Either<Failure, Account?>> getAccountSession() =>
      callDataSource(() => _localDatasource.getCurrentUser());

  @override
  Future<Either<Failure, void>> syncData() =>
      callDataSource(() => _remoteDatasource.syncData());
}
