import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/network/supabase_module.dart';
import 'package:my_to_do/core/utils/date_util.dart';

abstract class SrtartupRemoteDatasource {
  Future<void> syncData();
}

@Injectable(as: SrtartupRemoteDatasource)
class SrtartupRemoteDatasourceImpl extends SrtartupRemoteDatasource {
  final SupabaseModule _supabase = getIt<SupabaseModule>();

  @override
  Future<void> syncData() async {
    await _supabase.client.from('to_do_table').delete().lt(
        'date',
        DateUtil.getSimpleFormattedDate(
            DateTime.now().subtract(const Duration(days: 1))));
  }
}
