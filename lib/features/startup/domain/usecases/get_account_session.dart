import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/startup/domain/repositories/startup_repository.dart';

@Injectable()
class GetAcccountSession {
  final StartupRepository _repo = getIt<StartupRepository>();

  Future<Either<Failure, Account?>> execute() => _repo.getAccountSession();
}
