import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/startup/domain/repositories/startup_repository.dart';

@Injectable()
class SyncData {
  final StartupRepository _repo = getIt<StartupRepository>();

  Future<Either<Failure, void>> execute() => _repo.syncData();
}
