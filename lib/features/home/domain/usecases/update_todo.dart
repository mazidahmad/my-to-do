import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/home/domain/repositories/home_repository.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';

@Injectable()
class UpdateTodo {
  final HomeRepository _repo = getIt<HomeRepository>();

  Future<Either<Failure, void>> execute(Todo todo) => _repo.updateTodo(todo);
}
