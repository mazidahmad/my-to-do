import 'package:dartz/dartz.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';

abstract class HomeRepository {
  Future<Either<Failure, List<Todo>>> getTodayTodoList(bool isCompleted);
  Future<Either<Failure, List<Todo>>> getTomorrowTodoList();
  Future<Either<Failure, List<Todo>>> getYesterdayTodoList(bool isCompleted);
  Future<Either<Failure, void>> updateCompletedTodo(int id, bool isCompleted);
  Future<Either<Failure, void>> updateTodo(Todo todo);
  Future<Either<Failure, void>> deleteTodo(int id);
}
