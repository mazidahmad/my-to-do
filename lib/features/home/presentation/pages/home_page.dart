import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/domain/entities/account.dart';
import 'package:my_to_do/core/widget/button/app_floating_button.dart';
import 'package:my_to_do/features/home/presentation/cubit/home_cubit.dart';
import 'package:my_to_do/features/home/presentation/widget/app_navigation_bar.dart';
import 'package:my_to_do/features/home/presentation/widget/home_header_section.dart';
import 'package:my_to_do/features/home/presentation/widget/pager/pager.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({required this.account, super.key});

  static const String routeName = '/home';
  final Account account;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final HomeCubit _cubit;
  int _currIndex = 1;

  final List<Widget> _pagers = const [
    YesterdayPager(),
    TodayPager(),
    TomorrowPager()
  ];

  @override
  void initState() {
    _cubit = getIt<HomeCubit>();
    super.initState();
  }

  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit
        ..geTodayTodoList()
        ..geTodayTodoCompletedList()
        ..getYesterdayTodoList()
        ..getYesterdayTodoCompletedList()
        ..getTomorrowTodoList(),
      child: Scaffold(
        floatingActionButton: const AppFloatingButton(),
        bottomNavigationBar: AppNavigationBar(
          currIndex: _currIndex,
          onChangedIndex: (idx) => setState(() {
            _currIndex = idx;
          }),
        ),
        body: BlocListener<HomeCubit, HomeState>(
          listener: (context, state) {},
          child: SafeArea(
              child: Column(
            children: [
              Gap(32.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.w),
                child: HomeHeaderSection(
                  displayName: widget.account.displayName.split(' ').first,
                  profilePicture: widget.account.profilePicture,
                ),
              ),
              Gap(32.h),
              Expanded(child: _pagers[_currIndex]),
            ],
          )),
        ),
      ),
    );
  }
}
