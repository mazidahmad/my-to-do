part of 'home_cubit.dart';

enum HomeStateStatus { initial, loading, loaded, failed }

class HomeState extends Equatable {
  final HomeStateStatus status;
  final List<Todo> todayPlanTodos;
  final List<Todo> todayCompletedTodos;
  final List<Todo> yesterdayPlanTodos;
  final List<Todo> yesterdayCompletedTodos;
  final List<Todo> tomorrowTodos;
  final Failure? failure;

  const HomeState(
      {required this.status,
      required this.todayPlanTodos,
      required this.todayCompletedTodos,
      required this.yesterdayPlanTodos,
      required this.yesterdayCompletedTodos,
      required this.tomorrowTodos,
      this.failure});

  @override
  List<Object?> get props {
    return [
      status,
      todayPlanTodos,
      todayCompletedTodos,
      yesterdayPlanTodos,
      yesterdayCompletedTodos,
      tomorrowTodos,
      failure,
    ];
  }

  HomeState copyWith({
    HomeStateStatus? status,
    List<Todo>? todayPlanTodos,
    List<Todo>? todayCompletedTodos,
    List<Todo>? yesterdayPlanTodos,
    List<Todo>? yesterdayCompletedTodos,
    List<Todo>? tomorrowTodos,
    Failure? failure,
  }) {
    return HomeState(
      status: status ?? this.status,
      todayPlanTodos: todayPlanTodos ?? this.todayPlanTodos,
      todayCompletedTodos: todayCompletedTodos ?? this.todayCompletedTodos,
      yesterdayPlanTodos: yesterdayPlanTodos ?? this.yesterdayPlanTodos,
      yesterdayCompletedTodos:
          yesterdayCompletedTodos ?? this.yesterdayCompletedTodos,
      tomorrowTodos: tomorrowTodos ?? this.tomorrowTodos,
      failure: failure ?? this.failure,
    );
  }
}
