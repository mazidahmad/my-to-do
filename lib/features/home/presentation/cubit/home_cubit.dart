import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/core/utils/date_util.dart';
import 'package:my_to_do/features/home/domain/usecases/delete_todo.dart';
import 'package:my_to_do/features/home/domain/usecases/get_today_todo_list.dart';
import 'package:my_to_do/features/home/domain/usecases/get_tomorrow_todo_list.dart';
import 'package:my_to_do/features/home/domain/usecases/get_yesterday_todo_list.dart';
import 'package:my_to_do/features/home/domain/usecases/update_completed_todo.dart';
import 'package:my_to_do/features/home/domain/usecases/update_todo.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';

part 'home_state.dart';

@Injectable()
class HomeCubit extends Cubit<HomeState> {
  HomeCubit()
      : super(
          const HomeState(
            status: HomeStateStatus.initial,
            todayPlanTodos: [],
            todayCompletedTodos: [],
            tomorrowTodos: [],
            yesterdayCompletedTodos: [],
            yesterdayPlanTodos: [],
          ),
        );

  final GetTodayTodoList _getTodayTodoList = getIt<GetTodayTodoList>();
  final GetYesterdayTodoList _getYesterdayTodoList =
      getIt<GetYesterdayTodoList>();
  final GetTomorrowTodoList _getTomorrowTodoList = getIt<GetTomorrowTodoList>();
  final UpdateCompletedTodo _updateCompletedTodo = getIt<UpdateCompletedTodo>();
  final UpdateTodo _updateTodo = getIt<UpdateTodo>();
  final DeleteTodo _deleteTodo = getIt<DeleteTodo>();

  void geTodayTodoList() async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var result = await _getTodayTodoList.execute(false);

    emit(
      result.fold(
        (failure) =>
            state.copyWith(status: HomeStateStatus.failed, failure: failure),
        (data) {
          return state.copyWith(
              status: HomeStateStatus.loaded, todayPlanTodos: data);
        },
      ),
    );
  }

  void geTodayTodoCompletedList() async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var result = await _getTodayTodoList.execute(true);

    emit(
      result.fold(
        (failure) =>
            state.copyWith(status: HomeStateStatus.failed, failure: failure),
        (data) {
          return state.copyWith(
              status: HomeStateStatus.loaded, todayCompletedTodos: data);
        },
      ),
    );
  }

  void getYesterdayTodoList() async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var result = await _getYesterdayTodoList.execute(false);

    emit(
      result.fold(
        (failure) =>
            state.copyWith(status: HomeStateStatus.failed, failure: failure),
        (data) {
          return state.copyWith(
              status: HomeStateStatus.loaded, yesterdayPlanTodos: data);
        },
      ),
    );
  }

  void getYesterdayTodoCompletedList() async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var result = await _getYesterdayTodoList.execute(true);

    emit(
      result.fold(
        (failure) =>
            state.copyWith(status: HomeStateStatus.failed, failure: failure),
        (data) {
          return state.copyWith(
              status: HomeStateStatus.loaded, yesterdayCompletedTodos: data);
        },
      ),
    );
  }

  void getTomorrowTodoList() async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var result = await _getTomorrowTodoList.execute();

    emit(
      result.fold(
        (failure) =>
            state.copyWith(status: HomeStateStatus.failed, failure: failure),
        (data) {
          return state.copyWith(
              status: HomeStateStatus.loaded, tomorrowTodos: data);
        },
      ),
    );
  }

  void updateCompletedTodo(int id, bool isCompleted) async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    List<Todo> currTodayList = [...state.todayPlanTodos];
    List<Todo> currTodayCompletedList = [...state.todayCompletedTodos];
    late Todo todo;

    if (!isCompleted) {
      todo = state.todayCompletedTodos
          .firstWhere((element) => element.id == id)
          .copyWith(isCompleted: isCompleted);
      currTodayCompletedList.removeWhere((element) => element.id == id);
      currTodayList.add(todo);
    } else {
      todo = state.todayPlanTodos
          .firstWhere((element) => element.id == id)
          .copyWith(isCompleted: isCompleted);
      currTodayList.removeWhere((element) => element.id == id);
      currTodayCompletedList.add(todo);
    }
    emit(state.copyWith(
        status: HomeStateStatus.loaded,
        todayPlanTodos: currTodayList,
        todayCompletedTodos: currTodayCompletedList));

    var result = await _updateCompletedTodo.execute(id, isCompleted);

    result.fold(
      (failure) => emit(
          state.copyWith(failure: failure, status: HomeStateStatus.failed)),
      (_) {},
    );
  }

  void moveToTodayTodoList(int id, DateTime date) async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var currTodayTodos = [...state.todayPlanTodos];
    var currYesterdayTodos = [...state.yesterdayPlanTodos];
    var currTomorrowTodos = [...state.tomorrowTodos];

    late Todo currTodo;

    if (DateUtil.isYesterday(date)) {
      currTodo = state.yesterdayPlanTodos
          .firstWhere((element) => element.id == id)
          .copyWith(date: DateTime.now());

      currYesterdayTodos.removeWhere((element) => element.id == id);
      currTodayTodos.add(currTodo);
    } else {
      currTodo = state.tomorrowTodos
          .firstWhere((element) => element.id == id)
          .copyWith(date: DateTime.now());

      currTomorrowTodos.removeWhere((element) => element.id == id);
      currTodayTodos.add(currTodo);
    }

    emit(state.copyWith(
        status: HomeStateStatus.loaded,
        tomorrowTodos: currTomorrowTodos,
        todayPlanTodos: currTodayTodos,
        yesterdayPlanTodos: currYesterdayTodos));

    var result = await _updateTodo.execute(currTodo);

    result.fold(
      (failure) => emit(
          state.copyWith(status: HomeStateStatus.failed, failure: failure)),
      (data) {},
    );
  }

  void deleteTodo(int id, bool isToday) async {
    emit(state.copyWith(status: HomeStateStatus.loading));

    var currTodayTodos = [...state.todayPlanTodos];
    var currTomorrowTodos = [...state.tomorrowTodos];

    if (isToday) {
      currTodayTodos.removeWhere((element) => element.id == id);
    } else {
      currTomorrowTodos.removeWhere((element) => element.id == id);
    }

    emit(state.copyWith(
      status: HomeStateStatus.loaded,
      tomorrowTodos: currTomorrowTodos,
      todayPlanTodos: currTodayTodos,
    ));

    var result = await _deleteTodo.execute(id);

    result.fold(
      (failure) => emit(
          state.copyWith(status: HomeStateStatus.failed, failure: failure)),
      (data) {},
    );
  }
}
