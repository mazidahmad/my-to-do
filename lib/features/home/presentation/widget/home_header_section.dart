import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/core/utils/date_util.dart';

class HomeHeaderSection extends StatelessWidget {
  const HomeHeaderSection({
    super.key,
    required this.displayName,
    required this.profilePicture,
  });

  final String displayName;
  final String profilePicture;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Hello, $displayName',
                style: AppTextStyle.displayMedium(),
              ),
              Gap(4.h),
              Text(
                DateUtil.getFormattedDate(DateTime.now()),
                style: AppTextStyle.bodySmall(
                    color: AppColors.textColor.withAlpha(120)),
              )
            ],
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            profilePicture,
            width: 40.w,
            height: 40.h,
          ),
        ),
      ],
    );
  }
}
