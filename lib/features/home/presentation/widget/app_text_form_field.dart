import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/theme/theme.dart';

class AppTextFormField extends StatelessWidget {
  const AppTextFormField({
    this.label,
    this.controller,
    this.hint,
    super.key,
    this.isExpand = false,
    this.onTap,
    this.isReadOnly,
    this.validator,
  });

  final String? hint;
  final String? label;
  final TextEditingController? controller;
  final bool isExpand;
  final void Function()? onTap;
  final bool? isReadOnly;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (label != null)
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                label!,
                style: AppTextStyle.bodySmall().copyWith(height: 0),
              ),
              Gap(8.h),
            ],
          ),
        SizedBox(
          height: isExpand ? 200.h : null,
          child: TextFormField(
            controller: controller,
            onTap: onTap,
            validator: validator,
            readOnly: isReadOnly ?? false,
            style: AppTextStyle.bodyLarge(),
            expands: isExpand,
            maxLines: isExpand ? null : 1,
            minLines: isExpand ? null : 1,
            textAlignVertical: TextAlignVertical.top,
            decoration: InputDecoration(
              hintText: hint,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 15.w, vertical: 17.h),
              filled: true,
              floatingLabelBehavior: FloatingLabelBehavior.always,
              hintStyle: AppTextStyle.bodyLarge(color: AppColors.hintTextColor)
                  .copyWith(height: 0),
              fillColor: AppColors.textfieldBackgroundColor,
              focusColor: AppColors.textfieldBackgroundColor,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.sp),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
