import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/features/home/presentation/cubit/home_cubit.dart';
import 'package:my_to_do/features/home/presentation/widget/pager/todo_list_section.dart';

class TodayPager extends StatefulWidget {
  const TodayPager({super.key});

  @override
  State<TodayPager> createState() => _TodayPagerState();
}

class _TodayPagerState extends State<TodayPager> {
  late final HomeCubit _cubit;

  bool isExpandedTodo = false;
  bool isExpandedCompletedTodo = false;

  @override
  void initState() {
    _cubit = context.read<HomeCubit>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: _cubit,
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Text(
                'Today Task',
                style: AppTextStyle.headlineLarge(),
              ),
            ),
            Visibility(
              visible: !isExpandedCompletedTodo,
              child: ToDoListSection(
                isLoading: state.status == HomeStateStatus.loading &&
                    state.todayPlanTodos.isEmpty,
                onExpand: (isExpand) => setState(() {
                  isExpandedTodo = isExpand;
                }),
                isExpand: isExpandedTodo,
                todos: state.todayPlanTodos.reversed.toList(),
              ),
            ),
            Visibility(
              visible: !isExpandedTodo,
              child: ToDoListSection(
                isLoading: state.status == HomeStateStatus.loading &&
                    state.todayCompletedTodos.isEmpty,
                onExpand: (isExpand) => setState(() {
                  isExpandedCompletedTodo = isExpand;
                }),
                isExpand: isExpandedCompletedTodo,
                todos: state.todayCompletedTodos.reversed.toList(),
                isCompleted: true,
              ),
            ),
          ],
        );
      },
    );
  }
}
