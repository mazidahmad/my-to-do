import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/features/home/presentation/widget/todo_list_item.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';

class ToDoListSection extends StatelessWidget {
  const ToDoListSection({
    required this.todos,
    this.isLoading = false,
    this.isExpand = false,
    this.isCompleted = false,
    this.onExpand,
    super.key,
  });

  final bool isLoading;
  final bool isCompleted;
  final List<Todo> todos;
  final bool isExpand;
  final void Function(bool isExpand)? onExpand;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                Gap(24.h),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  child: Row(
                    children: [
                      Text(
                        isCompleted ? 'Completed' : 'Plans',
                        style: AppTextStyle.headlineLarge(),
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () => onExpand?.call(!isExpand),
                        child: Text(
                          isExpand ? 'View Less' : 'View All',
                          style: AppTextStyle.bodyMedium(
                            color: AppColors.textColor.withAlpha(120),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Gap(12.h),
                Expanded(
                  child: (todos.isNotEmpty)
                      ? ListView.builder(
                          physics: isExpand
                              ? null
                              : const NeverScrollableScrollPhysics(),
                          itemCount: isExpand
                              ? todos.length
                              : todos.length > 3
                                  ? 3
                                  : todos.length,
                          itemBuilder: (context, index) {
                            return ToDoListItem(
                              todo: todos[index],
                            );
                          },
                        )
                      : Center(
                          child: Text(
                            'No To Do Here',
                            style: AppTextStyle.headlineMedium(),
                          ),
                        ),
                ),
              ],
            ),
    );
  }
}
