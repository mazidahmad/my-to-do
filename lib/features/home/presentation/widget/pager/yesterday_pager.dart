import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/features/home/presentation/cubit/home_cubit.dart';
import 'package:my_to_do/features/home/presentation/widget/pager/todo_list_section.dart';

class YesterdayPager extends StatefulWidget {
  const YesterdayPager({super.key});

  @override
  State<YesterdayPager> createState() => _YesterdayPagerState();
}

class _YesterdayPagerState extends State<YesterdayPager> {
  bool isExpandedTodo = false;
  bool isExpandedCompletedTodo = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: context.read<HomeCubit>(),
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Text(
                'Yesterday Task',
                style: AppTextStyle.headlineLarge(),
              ),
            ),
            Visibility(
              visible: !isExpandedCompletedTodo,
              child: ToDoListSection(
                isLoading: state.status == HomeStateStatus.loading &&
                    state.yesterdayPlanTodos.isEmpty,
                onExpand: (isExpand) => setState(() {
                  isExpandedTodo = isExpand;
                }),
                isExpand: isExpandedTodo,
                todos: state.yesterdayPlanTodos.reversed.toList(),
              ),
            ),
            Visibility(
              visible: !isExpandedTodo,
              child: ToDoListSection(
                isLoading: state.status == HomeStateStatus.loading &&
                    state.yesterdayCompletedTodos.isEmpty,
                onExpand: (isExpand) => setState(() {
                  isExpandedCompletedTodo = isExpand;
                }),
                isExpand: isExpandedCompletedTodo,
                todos: state.yesterdayCompletedTodos.reversed.toList(),
                isCompleted: true,
              ),
            ),
          ],
        );
      },
    );
  }
}
