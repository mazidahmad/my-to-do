import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/features/home/presentation/cubit/home_cubit.dart';
import 'package:my_to_do/features/home/presentation/widget/pager/todo_list_section.dart';

class TomorrowPager extends StatelessWidget {
  const TomorrowPager({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: context.read<HomeCubit>(),
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Text(
                'Tomorow Task',
                style: AppTextStyle.headlineLarge(),
              ),
            ),
            ToDoListSection(
              isLoading: state.status == HomeStateStatus.loading &&
                  state.tomorrowTodos.isEmpty,
              isExpand: true,
              todos: state.tomorrowTodos,
            ),
          ],
        );
      },
    );
  }
}
