import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:gap/gap.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/core/utils/date_util.dart';
import 'package:my_to_do/features/home/presentation/cubit/home_cubit.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/router/app_router.dart';
import 'package:my_to_do/router/app_router.gr.dart';

class ToDoListItem extends StatelessWidget {
  const ToDoListItem({
    required this.todo,
    super.key,
  });

  final Todo todo;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 10.h),
      child: Slidable(
        enabled: !DateUtil.isYesterday(todo.date) && !todo.isCompleted,
        endActionPane: ActionPane(
          extentRatio: 1 / 4,
          motion: const ScrollMotion(),
          children: [
            SlidableAction(
              borderRadius: BorderRadius.circular(20),
              padding: EdgeInsets.symmetric(vertical: 10.h),
              onPressed: (_) => context
                  .read<HomeCubit>()
                  .deleteTodo(todo.id, DateUtil.isToday(todo.date)),
              foregroundColor: AppColors.red,
              icon: Icons.delete_outlined,
            ),
          ],
        ),
        child: GestureDetector(
          onTap: () =>
              getIt<AppRouter>().push(CreateTodoRoute(todo: todo)).then(
                    (value) => context.read<HomeCubit>()
                      ..geTodayTodoList()
                      ..getTomorrowTodoList(),
                  ),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 13.h),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.sp),
              boxShadow: [
                BoxShadow(
                  color: AppColors.textColor.withOpacity(0.15),
                  blurRadius: 12.sp,
                  offset: Offset(0, 4.h),
                ),
              ],
              border: Border.all(
                  color: todo.isCompleted
                      ? AppColors.green
                      : AppColors.surfaceColor,
                  width: 2.w,
                  style: BorderStyle.solid),
            ),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    if (DateUtil.isToday(todo.date)) {
                      context
                          .read<HomeCubit>()
                          .updateCompletedTodo(todo.id, !todo.isCompleted);
                    }
                  },
                  child: Icon(
                    todo.isCompleted
                        ? Icons.check_circle_outline_rounded
                        : Icons.circle_outlined,
                    color: todo.isCompleted
                        ? AppColors.green
                        : AppColors.surfaceColor,
                    size: 30.sp,
                  ),
                ),
                Gap(18.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        todo.title,
                        style: AppTextStyle.headlineMedium(),
                      ),
                      Gap(8.h),
                      Text(
                        DateUtil.getFormattedDate(todo.date),
                        style: AppTextStyle.bodySmall(
                            color: AppColors.textColor.withAlpha(100)),
                      ),
                    ],
                  ),
                ),
                Gap(10.w),
                GestureDetector(
                  onTap: () {
                    if (DateUtil.isToday(todo.date)) {
                      context
                          .read<HomeCubit>()
                          .updateCompletedTodo(todo.id, !todo.isCompleted);
                    } else if (!todo.isCompleted) {
                      context
                          .read<HomeCubit>()
                          .moveToTodayTodoList(todo.id, todo.date);
                    }
                  },
                  child: Icon(
                    DateUtil.getIconStatusByDate(todo.date, todo.isCompleted),
                    color: todo.isCompleted
                        ? AppColors.red
                        : AppColors.primaryColor,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
