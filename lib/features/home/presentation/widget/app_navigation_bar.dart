import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_to_do/core/theme/theme.dart';
import 'package:my_to_do/features/home/presentation/widget/app_navigation_item.dart';

class AppNavigationBar extends StatelessWidget {
  const AppNavigationBar(
      {super.key, required this.currIndex, required this.onChangedIndex});

  final int currIndex;
  final void Function(int idx) onChangedIndex;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: AppColors.surfaceColor,
      elevation: 0,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            AppNavigationItem(
              title: 'Yesterday',
              isActive: currIndex == 0,
              onPressed: () => onChangedIndex.call(0),
            ),
            AppNavigationItem(
              title: 'Today',
              isActive: currIndex == 1,
              onPressed: () => onChangedIndex.call(1),
            ),
            AppNavigationItem(
              title: 'Tomorrow',
              isActive: currIndex == 2,
              onPressed: () => onChangedIndex.call(2),
            ),
          ],
        ),
      ),
    );
  }
}
