import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/network/supabase_module.dart';
import 'package:my_to_do/core/utils/date_util.dart';
import 'package:my_to_do/features/todo/data/models/todo_model.dart';

abstract class HomeRemoteDatasource {
  Future<List<TodoModel>> getTodayTodoList(String email, bool isCompleted);
  Future<List<TodoModel>> getYesterdayTodoList(String email, bool isCompleted);
  Future<List<TodoModel>> getTomorrowTodoList(String email);
  Future<void> updateCompletedTodo(int id, bool isCompleted);
  Future<void> updateTodo(TodoModel todo);
  Future<void> deleteTodo(int id);
}

@Injectable(as: HomeRemoteDatasource)
class HomeRemoteDatasourceImpl extends HomeRemoteDatasource {
  final SupabaseModule _supabase = getIt<SupabaseModule>();

  @override
  Future<void> deleteTodo(int id) async {
    await _supabase.client.from('to_do_table').delete().eq('id', id);
  }

  @override
  Future<List<TodoModel>> getTodayTodoList(
      String email, bool isCompleted) async {
    var response = await _supabase.client
        .from('to_do_table')
        .select()
        .eq('email', email)
        .eq('date', DateUtil.getSimpleFormattedDate(DateTime.now()))
        .eq('is_completed', isCompleted);
    return List.from(response)
        .cast<Map<String, dynamic>>()
        .map((element) => TodoModel.fromMap(element))
        .toList();
  }

  @override
  Future<List<TodoModel>> getTomorrowTodoList(String email) async {
    var response = await _supabase.client
        .from('to_do_table')
        .select()
        .eq('email', email)
        .gt('date', DateUtil.getSimpleFormattedDate(DateTime.now()));
    return List.from(response)
        .cast<Map<String, dynamic>>()
        .map((element) => TodoModel.fromMap(element))
        .toList();
  }

  @override
  Future<List<TodoModel>> getYesterdayTodoList(
      String email, bool isCompleted) async {
    var response = await _supabase.client
        .from('to_do_table')
        .select()
        .eq('email', email)
        .eq('is_completed', isCompleted)
        .lt('date', DateUtil.getSimpleFormattedDate(DateTime.now()));
    return List.from(response)
        .cast<Map<String, dynamic>>()
        .map((element) => TodoModel.fromMap(element))
        .toList();
  }

  @override
  Future<void> updateCompletedTodo(int id, bool isCompleted) async {
    await _supabase.client
        .from('to_do_table')
        .update({'is_completed': isCompleted}).eq('id', id);
  }

  @override
  Future<void> updateTodo(TodoModel todo) async {
    await _supabase.client
        .from('to_do_table')
        .update(todo.toMap())
        .eq('id', todo.id);
  }
}
