import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:my_to_do/core/data/datasources/local_datasource.dart';
import 'package:my_to_do/core/di/service_locator.dart';
import 'package:my_to_do/core/error/failures.dart';
import 'package:my_to_do/core/mixin/repository_mixin.dart';
import 'package:my_to_do/features/home/data/datasources/home_remote_datasource.dart';
import 'package:my_to_do/features/home/domain/repositories/home_repository.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';

@Injectable(as: HomeRepository)
class HomeRepositoryImpl extends HomeRepository with RepositoryMixin {
  final HomeRemoteDatasource _remoteDatasource = getIt<HomeRemoteDatasource>();
  final LocalDatasource _localDatasource = getIt<LocalDatasource>();

  @override
  Future<Either<Failure, void>> deleteTodo(int id) =>
      callDataSource(() => _remoteDatasource.deleteTodo(id));

  @override
  Future<Either<Failure, List<Todo>>> getTodayTodoList(bool isCompleted) =>
      callDataSource(() async {
        var account = await _localDatasource.getCurrentUser();

        return await _remoteDatasource.getTodayTodoList(
            account!.email, isCompleted);
      });

  @override
  Future<Either<Failure, List<Todo>>> getTomorrowTodoList() =>
      callDataSource(() async {
        var account = await _localDatasource.getCurrentUser();

        return await _remoteDatasource.getTomorrowTodoList(account!.email);
      });

  @override
  Future<Either<Failure, List<Todo>>> getYesterdayTodoList(bool isCompleted) =>
      callDataSource(() async {
        var account = await _localDatasource.getCurrentUser();

        return await _remoteDatasource.getYesterdayTodoList(
            account!.email, isCompleted);
      });

  @override
  Future<Either<Failure, void>> updateCompletedTodo(int id, bool isCompleted) =>
      callDataSource(
          () => _remoteDatasource.updateCompletedTodo(id, isCompleted));

  @override
  Future<Either<Failure, void>> updateTodo(Todo todo) =>
      callDataSource(() => _remoteDatasource.updateTodo(todo.toModel()));
}
