import 'package:http/http.dart';
import 'package:mocktail/mocktail.dart';
import 'package:my_to_do/core/data/datasources/local_datasource.dart';
import 'package:my_to_do/core/network/supabase_module.dart';
import 'package:my_to_do/core/storage/secure_storage.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class MockHttpClient extends Mock implements Client {
  final Response response;

  MockHttpClient({required this.response});

  @override
  Future<Response> head(Uri url, {Map<String, String>? headers}) async {
    return response;
  }
}

class MockSecureStorage extends Mock implements SecureStorage {}

class MockSupabaseModule extends Mock implements SupabaseModule {}

class MockLocalDatasource extends Mock implements LocalDatasource {}

class MockSupabaseClient extends Mock implements SupabaseClient {}

class MockSupabaseQueryBuilder extends Mock implements SupabaseQueryBuilder {
  final PostgrestFilterBuilder<dynamic> result;

  MockSupabaseQueryBuilder({required this.result});

  @override
  PostgrestFilterBuilder insert(values, {bool defaultToNull = true}) {
    return result;
  }

  @override
  PostgrestFilterBuilder<R> select<R>(
      [String columns = '*', FetchOptions options = const FetchOptions()]) {
    throw UnimplementedError();
  }

  @override
  PostgrestFilterBuilder update(Map values,
      {FetchOptions options = const FetchOptions()}) {
    return result;
  }
}
