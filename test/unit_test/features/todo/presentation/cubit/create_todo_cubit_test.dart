import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:my_to_do/features/home/domain/usecases/update_todo.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/features/todo/domain/usecases/create_todo.dart';
import 'package:my_to_do/features/todo/presentation/cubit/create_todo_cubit.dart';

import '../../../../../helpers/test_injection.dart';

class MockCreateTodo extends Mock implements CreateTodo {}

class MockUpdateTodo extends Mock implements UpdateTodo {}

void main() {
  late CreateTodo mockCreateTodo;
  late CreateTodoCubit cubit;
  late UpdateTodo mockUpdateTodo;

  setUp(() {
    mockCreateTodo = MockCreateTodo();
    mockUpdateTodo = MockUpdateTodo();
    registerTestFactory<CreateTodo>(mockCreateTodo);
    registerTestFactory<UpdateTodo>(mockUpdateTodo);
    cubit = CreateTodoCubit();
  });

  tearDown(() {
    cubit.close();
  });

  test('initial CreateTodoCubit state should be initial', () {
    expect(cubit.state, CreateTodoInitial());
  });

  group(
    'Create Todo',
    () {
      var date = DateTime.parse('2022-01-01');
      var request = Todo(
          title: 'title',
          date: date,
          isCompleted: false,
          description: 'description');

      blocTest<CreateTodoCubit, CreateTodoState>(
        'emits [loading, success] when success.',
        build: () {
          when(
            () => mockCreateTodo.execute(request),
          ).thenAnswer((_) async => const Right(null));
          return cubit;
        },
        wait: const Duration(milliseconds: 100),
        act: (cubit) {
          cubit.titleController.text = 'title';
          cubit.descriptionController.text = 'description';
          cubit.createTodo(date);
        },
        expect: () => <CreateTodoState>[
          CreateTodoLoading(),
          CreateTodoSuccess(),
        ],
        verify: (bloc) {
          verify(
            () => mockCreateTodo.execute(request),
          );
        },
      );
    },
  );
}
