import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:my_to_do/features/todo/domain/entities/todo.dart';
import 'package:my_to_do/features/todo/domain/repositories/todo_repository.dart';
import 'package:my_to_do/features/todo/domain/usecases/create_todo.dart';

import '../../../../../helpers/test_injection.dart';

class MockToDoRepository extends Mock implements TodoRepository {}

void main() {
  late CreateTodo usecase;
  late TodoRepository mockDatasource;

  setUpAll(
    () {
      mockDatasource = MockToDoRepository();
      registerTestFactory<TodoRepository>(mockDatasource);
      usecase = CreateTodo();
    },
  );

  group(
    'Create ToDo',
    () {
      var entity = Todo(
          id: 1,
          email: 'email',
          title: 'title',
          date: DateTime.parse('2022-01-01'),
          isCompleted: false);

      test(
        'should return Right void when success',
        () async {
          when(() => mockDatasource.createTodo(entity)).thenAnswer(
            (_) async => const Right(null),
          );

          var result = await usecase.execute(entity);

          verify(() => mockDatasource.createTodo(entity));

          expect(result, equals(const Right(null)));
        },
      );
    },
  );
}
