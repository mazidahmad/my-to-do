import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:my_to_do/features/todo/data/datasources/todo_remote_datasource.dart';
import 'package:my_to_do/features/todo/data/models/todo_model.dart';
import 'package:my_to_do/features/todo/data/repositories/todo_repository_impl.dart';
import 'package:my_to_do/features/todo/domain/repositories/todo_repository.dart';

import '../../../../../helpers/test_injection.dart';

class MockToDoRemoteDatasource extends Mock implements ToDoRemoteDatasource {}

void main() {
  late TodoRepository repository;
  late ToDoRemoteDatasource mockDatasource;

  setUpAll(
    () {
      mockDatasource = MockToDoRemoteDatasource();
      registerTestFactory<ToDoRemoteDatasource>(mockDatasource);
      repository = TodoRepositoryImpl();
    },
  );

  group(
    'Create ToDo',
    () {
      var model = TodoModel(
          id: 1,
          email: 'email',
          title: 'title',
          date: DateTime.parse('2022-01-01'),
          isCompleted: false);

      test(
        'should return Right void when success',
        () async {
          when(() => mockDatasource.createTodo(model)).thenAnswer(
            (_) async => model,
          );

          var result = await repository.createTodo(model);

          verify(() => mockDatasource.createTodo(model));

          expect(result, equals(Right(model)));
        },
      );
    },
  );
}
