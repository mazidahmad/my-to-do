import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mocktail/mocktail.dart';
import 'package:my_to_do/core/data/datasources/local_datasource.dart';
import 'package:my_to_do/core/data/models/account_model.dart';
import 'package:my_to_do/core/network/supabase_module.dart';
import 'package:my_to_do/features/todo/data/datasources/todo_remote_datasource.dart';
import 'package:my_to_do/features/todo/data/models/todo_model.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../../../../../helpers/mock_module.dart';
import '../../../../../helpers/test_injection.dart';

class MockQueryBuilder<T> extends Mock implements SupabaseQueryBuilder {
  final dynamic responseData;

  MockQueryBuilder({required this.responseData});

  @override
  PostgrestFilterBuilder<T> insert(
    dynamic values, {
    bool defaultToNull = true,
  }) {
    PostgrestFilterBuilder<T> response = PostgrestFilterBuilder(
      PostgrestBuilder(
        url: Uri.parse('supabase.io'),
        httpClient: MockHttpClient(
          response: Response(
            jsonEncode(responseData),
            200,
            request: Request(
              'POST',
              Uri.parse('supabase.io'),
            ),
          ),
        ),
        headers: {},
        body: responseData,
        options: const FetchOptions(head: true),
      ),
    );

    return PostgrestFilterBuilder<T>(response);
  }
}

void main() {
  late ToDoRemoteDatasource datasource;
  late SupabaseModule mockSupabaseModule;
  late LocalDatasource mockLocalDatsource;
  late SupabaseClient mockSupabaseClient;

  setUpAll(
    () {
      mockSupabaseModule = MockSupabaseModule();
      mockLocalDatsource = MockLocalDatasource();
      mockSupabaseClient = MockSupabaseClient();
      registerTestFactory<LocalDatasource>(mockLocalDatsource);
      registerTestFactory<SupabaseModule>(mockSupabaseModule);
      datasource = ToDoRemoteDatasourceImpl();
    },
  );

  group(
    'Create todo',
    () {
      var responseData = {
        'id': 1,
        'email': 'email@email.com',
        'title': 'title',
        'date': '2022-01-01',
        'description': 'description',
        'is_completed': false,
      };

      var account = const AccountModel(
          displayName: 'John Doe',
          email: 'email@email.com',
          profilePicture: 'profilePicture',
          idToken: 'idToken');

      var requestData = TodoModel(
          title: 'title',
          date: DateTime.parse('2022-01-01'),
          isCompleted: false,
          id: 1,
          description: 'description',
          email: 'email@email.com');

      test(
        'should success create todo when done',
        () async {
          when(() => mockLocalDatsource.getCurrentUser())
              .thenAnswer((_) async => account);
          when(() => mockSupabaseModule.client).thenReturn(mockSupabaseClient);
          when(() => mockSupabaseClient.from('to_do_table'))
              .thenAnswer((_) => MockQueryBuilder(responseData: responseData));

          await datasource.createTodo(requestData);

          verify(() => mockLocalDatsource.getCurrentUser());
          verify(() => mockSupabaseModule.client);
          verify(() => mockSupabaseClient.from('to_do_table'));
        },
      );
    },
  );
}
